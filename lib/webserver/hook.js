"use strict";

var CryptoJs = require("cryptojs");
var Sequelize = require("sequelize");
var Token = require("../db/token");
var User = require("../db/user");
var q = require("q");

var sequelize = new Sequelize('jocondelab', 'root', 'root', {
    host: '127.0.0.1',
    port: 3306,
    language: 'en',
    dialect: 'mysql',
    dialectOptions: {dialect: 'mariadb'}
});

var user = new User(sequelize);
var token = new Token(sequelize);

function generateToken(data) {
    return CryptoJs.enc.Base64.stringify(CryptoJs.SHA256(data));
}

exports.validateClient = function (clientId, clientSecret, cb) {
    var isValid = true ;//token.validate(clientId, clientSecret);
    cb(null, isValid);
};

exports.grantUserToken = function (username, password, cb) {
    var hash = CryptoJs.SHA256(password).toString();
    var future = q.defer();
    user.validate(future, username, password);
    future.promise.then(function (currentUser) {
        if (currentUser.password == hash) {
            var secret = generateToken(username + ":" + password);
            token.create(future, currentUser.id, secret);
            return future.promise.then(function (created) {
                return cb(null, secret);
            });
        }
        cb(null, false);
    });
};

exports.authenticateToken = function (secret, cb) {
    var future = q.defer();
    token.getUser(future, secret);
    return future.promise.then(function (id) {
        if (id) {
            return cb(null, id);
        }
    });

    cb(null, false);
};