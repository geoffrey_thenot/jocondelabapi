	'use strict';

var restify = require("restify");
var restifyOAuth2 = require("restify-oauth2");
var q = require("q");
var hook = require("./hook");
var _ = require('underscore');

var WebServer;

WebServer = function() { // export object dbServer pour l'utiliser avec require

};


WebServer.prototype.start = function (pubsub)	{
	this.server = restify.createServer({ name: "Joconde Lab API", version: "1.0.0",
	  	formatters: {
		    '*/*': function formatOpenData(req, res, body) {
		      if (body instanceof Error)
		        return body.stack;

		      if (Buffer.isBuffer(body)){
		      	console.log('body', body);
		      	return body.toString('base64');
		      }
		        
		      return JSON.stringify(body);
		    }
	  	}
	});
	
	this.RESOURCES = Object.freeze({
	    INITIAL: "/",
	    TOKEN: "/oauth2/token",
	    PUBLIC: "/public",
	    SECRET: "/secret"
	});

	this.server.use(restify.CORS()); // cross domain

	function unknownMethodHandler(req, res) {
	  	if (req.method.toLowerCase() === 'options') {
	    	var allowHeaders = ['Accept', 'Accept-Version', 'Content-Type', 'Api-Version', 'Authorization'];

	    	if (res.methods.indexOf('OPTIONS') === -1) res.methods.push('OPTIONS');

		    res.header('Access-Control-Allow-Credentials', true);
		    res.header('Access-Control-Allow-Headers', allowHeaders.join(', '));
		    res.header('Access-Control-Allow-Methods', res.methods.join(', '));
		    res.header('Access-Control-Allow-Origin', req.headers.origin);

	    	return res.send(204);
  		} else
	    	return res.send(new restify.MethodNotAllowedError());
	}

	this.server.on('MethodNotAllowed', unknownMethodHandler);
	this.server.use(restify.fullResponse());
	this.server.use(restify.queryParser());
	this.server.use(restify.bodyParser());

	/* SECURITY */
	this.server.use(restify.authorizationParser());
	restifyOAuth2.ropc(this.server, { tokenEndpoint: this.RESOURCES.TOKEN, hooks: hook });

	this.server.listen(9001, function () {
		console.log("WebServer started");
	});

	this.pubsub = pubsub;

	this.respondToGet();

	/****** SEARCH ******/
	this.autocomplete();
	this.search();
	this.searchEcho();

	/****** NOTICES *****/
	this.respondToNotice();
	this.respondToNotices();
	this.respondToNoticeSuggest();
	this.respondToEchoNotice();

	/******* USERS ******/
	this.respondToRegister();

	/***** FAVORITES *****/
	this.respondToFavorite();
	this.respondToAddFavorite();
	this.respondToRemoveFavorite();
	this.respondToFavoritedNotices();
	this.respondToFavoritedEchos();
	this.respondToIsFavorited();

	/******* ECHOS *******/
	this.respondToEcho();
	this.respondToEchos();
	this.respondToAddEcho();
};

/****** SEARCH ******/

WebServer.prototype.autocomplete = function () {
	this.server.get('/autocomplete/:search', function (req, res, next) {
		var futureRow = q.defer();
		this.pubsub.emit('autocomplete', req.params.search, futureRow, 10, "*", req.params.filter);

		futureRow.promise.then(function (row) {
			res.send(row);
		});
		next();
	}.bind(this));

};

WebServer.prototype.search = function () {
	this.server.get('/search/:search', function (req, res, next) {
		var futureRow = q.defer();
		this.pubsub.emit('search', req.params.search, futureRow, 10, req.params.filter);

		futureRow.promise.then(function (row) {
			res.send(row);
		});
		next();
	}.bind(this));

};

WebServer.prototype.searchEcho = function () {
	this.server.get('/search/echos/:search', function (req, res) {
		var futureRow = q.defer();
		this.pubsub.emit('searchEcho', req.params.search, futureRow, 10);

		futureRow.promise.then(function (row) {
			res.send(row);
		});

	}.bind(this));

};

/****** NOTICE ******/

WebServer.prototype.respondToNotice = function () {
	this.server.get('/notices/:id', function (req, res) {
		var futureRow = q.defer();
		this.pubsub.emit('notice', futureRow, req.params.id);

		futureRow.promise.then(function (row) {
			res.send(row);
		});

	}.bind(this));

};

WebServer.prototype.respondToNotices = function () {
	this.server.get('/notices', function (req, res) {
		var futureRow = q.defer();
		this.pubsub.emit('notices', futureRow, req.params.notices);

		futureRow.promise.then(function (row) {
			res.send(row);
		});

	}.bind(this));

};

WebServer.prototype.respondToNoticeSuggest = function () {
	this.server.get('/notices/:id/suggest', function (req, res) {
		var futureRow = q.defer();
		this.pubsub.emit('noticeSuggest', futureRow, req.params.terms);

		futureRow.promise.then(function (row) {
			res.send(row);
		});

	}.bind(this));

};

WebServer.prototype.respondToEchoNotice = function () {
	this.server.get('/notices/:id/echos', function (req, res) {
		var futureRow = q.defer();
		this.pubsub.emit('noticeEcho', futureRow, req.params.id, req.params.lang);

		futureRow.promise.then(function (row) {
			res.send(row);
		});

	}.bind(this));

};

/****** USERS ******/

WebServer.prototype.respondToRegister = function () {
	this.server.post('/user', function (req, res) {
		var futureRow = q.defer();
		this.pubsub.emit('register', futureRow, req.params.email, req.params.password);

		futureRow.promise.then(function (row) {
			res.send(row);
		});

	}.bind(this));

};

/****** FAVORITES ******/

WebServer.prototype.respondToFavorite = function () {
	this.server.get('/user/favorites', function (req, res) {
		if (!req.username) {
        	return res.sendUnauthorized();
    	}
    	var futureRow = q.defer();

    	this.pubsub.emit('favorites', futureRow, req.username);
    	futureRow.promise.then(function (row) {
			res.send(row);
		});

	}.bind(this));

};

WebServer.prototype.respondToFavoritedEchos = function () {
	this.server.get('/user/echos/favorites', function (req, res) {
		if (!req.username) {
        	return res.sendUnauthorized();
    	}
    	var futureRow = q.defer();
    	this.pubsub.emit('favoritedEchos', futureRow, req.params.echos);
    	futureRow.promise.then(function (row) {
			res.send(row);
		});

	}.bind(this));

};

WebServer.prototype.respondToFavoritedNotices = function () {
	this.server.get('/user/notices/favorites', function (req, res) {
		if (!req.username) {
        	return res.sendUnauthorized();
    	}
    	var futureRow = q.defer();
    	this.pubsub.emit('favoritedNotices', futureRow, req.params.notices);
    	futureRow.promise.then(function (row) {
			res.send(row);
		});

	}.bind(this));

};

WebServer.prototype.respondToIsFavorited = function () {
	this.server.get('/user/favorites/:id', function (req, res) {
		if (!req.username) {
        	return res.sendUnauthorized();
    	}
    	var futureRow = q.defer();
    	this.pubsub.emit('checkFavoriteId', futureRow, req.username, req.params.id, req.params.type);
    	futureRow.promise.then(function (row) {
    		console.log(row);
			res.send(row);
		});

	}.bind(this));

};

WebServer.prototype.respondToRemoveFavorite = function () {
	this.server.del('/user/favorites/:id', function (req, res) {
		if (!req.username) {
        	return res.sendUnauthorized();
    	}
    	var futureRow = q.defer();

    	this.pubsub.emit('removeFavorite', futureRow, req.username, req.params.id, req.params.type);

    	futureRow.promise.then(function (row) {
			res.send(row);
		});


	}.bind(this));

};

WebServer.prototype.respondToAddFavorite = function () {
	this.server.post('/user/favorites', function (req, res) {
		if (!req.username) {
        	return res.sendUnauthorized();
    	}
    	var futureRow = q.defer();
    	this.pubsub.emit('addFavorite', futureRow, req.username, req.params.id, req.params.type);

    	futureRow.promise.then(function (row) {
			res.send(row);
		});

	}.bind(this));

};

/****** SECURITY ******/
WebServer.prototype.respondToGet = function () {
	this.server.get(this.RESOURCES.INITIAL, function (req, res) {
	    var response = {
	        _links: {
	            self: { href: this.RESOURCES.INITIAL }
	        }
	    };

	    if (req.username) {
	        response._links["/secret"] = { href: this.RESOURCES.SECRET };
	    } else {
	        response._links["oauth2-token"] = {
	            href: this.RESOURCES.TOKEN,
	            "grant-types": "password",
	            "token-types": "bearer"
	        };
	    }

	    res.contentType = "application/hal+json";
	    res.send(response);
	});
};


/****** ECHO ******/

WebServer.prototype.respondToEcho = function () {
	this.server.get('/echos/:id', function (req, res) {
		var futureRow = q.defer();
		this.pubsub.emit('echo', futureRow, req.params.id, req.params.lang);

		futureRow.promise.then(function (row) {
			res.send(row);
		});

	}.bind(this));

};

WebServer.prototype.respondToEchos = function () {
	this.server.get('/echos', function (req, res) {
		var futureRow = q.defer();
		this.pubsub.emit('echos', futureRow, req.params.offset, req.params.limit, req.params.lang);

		futureRow.promise.then(function (row) {
			res.send(row);
		});

	}.bind(this));

};

WebServer.prototype.respondToAddEcho = function () {
	this.server.post('/echos', function (req, res) {
		console.log(req.files);
		var futureRow = q.defer();
		this.pubsub.emit('addEcho', futureRow, req.params.title, req.params.intro, 
						req.params.about, req.params.image, req.params.notices);

		futureRow.promise.then(function (row) {
			res.send(row);
		});

	}.bind(this));

};


module.exports = WebServer;