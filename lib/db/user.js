'use strict';

var Sequelize = require("sequelize");
var Cryptojs = require("cryptojs");
//var Favorite = require("./favorite");
var q = require("q");

var User = function (sequelize) {
	this.define(sequelize);
};

User.prototype.define = function (sequelize) {
	this.model = sequelize.define('user', {
		id: Sequelize.INTEGER,
		email: {
			type: Sequelize.STRING,
			validate: {
				isEmail: true
			}
		},
		password: Sequelize.STRING
	}, {
		
	});
	this.sequelize = sequelize;
	//this.favorite = new Favorite(sequelize).getModel();
	//this.model.hasMany(this.favorite, {as: 'Favorites'});
	return this.model;
}

User.prototype.getModel = function() {
	return this.model;
}

// User.prototype.addFavorite = function(defered, email, favoriteId) {
// 	this.model.find({where: {email: email}}).success(function (user) {
// 		this.favorite.create({ user_id: user.id, notice_id: favoriteId }).success(function(favorite, created) {
//     		console.log("create", created);
//     		//defered.resolve(true);
//     	});
// 	});
// }

User.prototype.getById = function (defered, id) {
	this.model.find(id).success(function (rows) {
		defered.resolve(rows);
	});
}

User.prototype.getByMail = function (defered, email) {
	this.model.find({ where: {email: emai} }).success(function(user) {
		defered.resolve(user);
	});
}

User.prototype.register = function (defered, email, password) {
	this.model.create({email: email, password: Cryptojs.SHA256(password).toString()}).success(function (user, created) {
		defered.resolve(created);
	});
}

User.prototype.login = function (defered, email, password) {
	var hash = Cryptojs.SHA256(password).toString();
	this.model.find({ where: {email: email} }).success(function (user) {
		defered.resolve(user.password == hash);
	});
}

User.prototype.validate = function (defered, email, password) {
	this.model.find({ where: {email: email} }).success(function (user) {
		defered.resolve(user);
	});
}

module.exports = User;