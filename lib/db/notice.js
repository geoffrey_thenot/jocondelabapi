'use strict';

var Sequelize = require("sequelize");

var Notice = function (sequelize) {
	this.define(sequelize);
};

Notice.prototype.define = function (sequelize) {
	this.model = sequelize.define('notice', {
		ref: Sequelize.STRING,
		aptn: Sequelize.STRING,
		dims: Sequelize.STRING,
		domn: Sequelize.STRING,
		www: Sequelize.STRING
	}, {
		freezeTableName: true, //doesn't add plural to table name
        tableName: 'core_notice',
        timestamps: false
	});
	return this.model;
}

Notice.prototype.terms = function (defered) {
	this.model.findAll({ where: ["id < ?", 25] }).success(function (rows) {
		defered.resolve(rows);
	});
}

Notice.prototype.term = function (defered, id) {
	this.model.find(id).success(function (rows) {
		defered.resolve(rows);
	});
}

module.exports = Notice;