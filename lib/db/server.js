'use strict';

var Sequelize = require("sequelize");
var User = require("./user");
//var Token = require("./token");
var Favorite = require("./favorite");
var Echo = require("./echo");

var DbServer;

DbServer = function () { // export object dbServer pour l'utiliser avec require
    
};

DbServer.prototype.start = function (pubsub)    {

    this.sequelize = new Sequelize('jocondelab', 'root', 'root', {
        host: '127.0.0.1',
        port: 3306,
        language: 'en',
        dialect: 'mysql',
        dialectOptions: {dialect: 'mariadb'}
    });

    var user = new User(this.sequelize);
    var favorite = new Favorite(this.sequelize);
    var echo = new Echo(this.sequelize);

    this.pubsub = pubsub;

    this.pubsub.on('register', function (defered, email, password) {
        user.register(defered, email, password);
    }.bind(this));

    this.pubsub.on('login', function (defered, email, password) {
        user.login(defered, email, password);
    }.bind(this));

    // this.pubsub.on('token', function (defered, email, secret) {
    //     token.validate(defered, email, secret);
    // }.bind(this));

    this.pubsub.on('addFavorite', function (defered, userId, objectId, type) {
        favorite.set(defered, userId, objectId, type);
    }.bind(this));

    this.pubsub.on('removeFavorite', function (defered, userId, objectId, type) {
        favorite.remove(defered, userId, objectId, type);
    }.bind(this));

    this.pubsub.on('favorites', function (defered, userId) {
        favorite.getAll(defered, userId);
    }.bind(this));

    this.pubsub.on('echo', function (defered, id, lang) {
        echo.get(defered, id, lang);
    }.bind(this));

    this.pubsub.on('echos', function (defered, offset, limit, lang) {
        echo.getAll(defered, offset, limit, lang);
    }.bind(this));

    this.pubsub.on('addEcho', function (defered, title, intro, content, about, image, notices) {
        echo.create(defered, title, intro, content, about, image, notices);
    }.bind(this));

    this.pubsub.on('favoritedEchos', function (defered, arrayId) {
        echo.getByIds(defered, arrayId);
    }.bind(this));

    this.pubsub.on('checkFavoriteId', function (defered, userId, objectId, type) {
        favorite.checkFavorite(defered, userId, objectId, type);
    }.bind(this));

    /* GET SEARCH ECHO */
    this.pubsub.on('searchEcho', function (val, defered, limit) {
        echo.search(defered, val, limit);
    }.bind(this));

    this.pubsub.on('noticeEcho', function (defered, noticeId, lang) {
        echo.getByNotice(defered, noticeId, lang);
    }.bind(this));

};

module.exports = DbServer;