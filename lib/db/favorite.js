'use strict';

var Sequelize = require("sequelize");
//var User = require("./user");

var Favorite = function (sequelize) {
	this.define(sequelize);
};

Favorite.prototype.define = function (sequelize) {
	this.model = sequelize.define('favorite', {
		id: Sequelize.INTEGER,
		object_id: Sequelize.INTEGER,
		user_id: Sequelize.INTEGER,
		type: Sequelize.INTEGER
	}, {
		timestamps: false
	});
	//this.user = new User(sequelize);
	//this.model.asOne(this.user, { foreignKey: 'user_id' });
	return this.model;
};

Favorite.prototype.getModel = function() {
	return this.model;
};

Favorite.prototype.set = function (defered, userId, objectId, type) {
	this.model.create({object_id: objectId, user_id: userId, type: type}).success(function (favorite, created) {
		defered.resolve(created);
	});
};

Favorite.prototype.getAll = function (defered, userId) {
	this.model.findAll({ where: { user_id: userId } }).success(function (favorites) {
		defered.resolve(favorites);
	});
};

Favorite.prototype.remove = function (defered,  userId, objectId, type) {
	console.log("message");
	this.model.find({ where: {object_id: objectId, user_id: userId, type: type} }).success(function (favorite) {
		favorite.destroy().success(function() {
			defered.resolve(true);
		});
	});
};

Favorite.prototype.checkFavorite = function(defered, userId, objectId, type) {
	this.model.find({ where: {user_id: userId , object_id: objectId, type: type} }).success(function (favorite) {
		defered.resolve({favorite: favorite === null ? false : true});
	});
};

module.exports = Favorite;