'use strict';

var Sequelize = require("sequelize");

var Echo = function (sequelize) {
	this.define(sequelize);
};

Echo.prototype.define = function (sequelize) {
	this.model = sequelize.define('echo', {
		id: Sequelize.INTEGER,
		number: Sequelize.INTEGER,
		subject: Sequelize.STRING,
		subject_more: Sequelize.STRING,
		title: Sequelize.STRING,
		intro: Sequelize.TEXT,
		content: Sequelize.TEXT,
		about_title: Sequelize.STRING,
		about: Sequelize.TEXT,
		image: Sequelize.STRING,
		lang: Sequelize.STRING
	}, {
		timestamps: true,
        tableName: 'echos'
	});

	this.notice = sequelize.define('Notice', {
        ref: Sequelize.STRING,
        aptn: Sequelize.STRING,
        dims: Sequelize.STRING,
        domn: Sequelize.STRING,
        www: Sequelize.STRING
    }, {
        freezeTableName: true, //doesn't add plural to table name
        tableName: 'core_notice',
        language: 'en',
        timestamps: false,
    });

   	this.model.hasMany(this.notice, {joinTableName: 'echos_notices', foreignKey: 'echo_id'});
  	this.notice.hasMany(this.model, {joinTableName: 'echos_notices', foreignKey: 'notice_id'});

	return this.model;
};

Echo.prototype.create = function (defered, title, intro, content, about, image, notices) {
	this.model.create({title: title, intro: intro, content: content, about: about, image: image})
		.success(function (echo, created) {
			defered.resolve(created);
		});
};

Echo.prototype.getAll = function (defered,  offset, limit, lang) {
	if (!lang) lang = 'fr';
	this.model.findAll({where: {lang: lang}, offset: offset, limit: limit, order: 'number DESC'})
		.success(function (rows) {
			defered.resolve(rows);
		});
};

Echo.prototype.get = function (defered, id, lang) {
	this.model.find({
		where: {number: id, lang: lang}, 
		include: [this.notice]
	}).success(function (rows) {
		defered.resolve(rows);
	});
};

Echo.prototype.getByIds = function (defered, arrayIds) {
	this.model.findAll({ where: {id: arrayIds} })
		.success( function (rows) {
			defered.resolve(rows);
		});
};

Echo.prototype.getByNotice = function (defered, noticeId, lang) {
	console.log("hello");
	this.model.find({ 
			where: ["core_notice.id = " + noticeId, {lang: lang}] ,
			include: [this.notice]
		})
		.success( function (rows) {
			console.log(rows);
			defered.resolve(rows);
		});
};

Echo.prototype.search = function (defered, val, limit) {
	this.model.find({
		where: ["subject LIKE '%" + val + "%'"]
	}).success(function (rows) {
		defered.resolve(rows);
	});
};


module.exports = Echo;