'use strict';

var Sequelize = require("sequelize");
var Cryptojs = require("cryptojs");

var Token = function (sequelize) {
	this.define(sequelize);
};

Token.prototype.define = function (sequelize) {
	this.model = sequelize.define('token', {
		id: Sequelize.INTEGER,
		user_id: Sequelize.INTEGER,
		token: Sequelize.STRING
	}, {
		timestamps: false
	});
	return this.model;
}

Token.prototype.create = function (defered, userId, secret) {
	this.model.create({user_id: userId, token: secret}).success(function (token, created) {
		defered.resolve(created);
	});
}

Token.prototype.validate = function (userId, secret) {
	this.model.find({ where: {user_id: userId} }).success(function (token) {
		return token.token == secret;
	});
}

Token.prototype.getUser = function (defered, secret) {
	this.model.find({ where: {token: secret} }).success(function (token) {
		defered.resolve(token.user_id);
	});
}

module.exports = Token;