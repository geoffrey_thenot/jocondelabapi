/*
 * api
 * https://github.com//api
 *
 * Copyright (c) 2013 
 * Licensed under the MIT license.
 */

'use strict';

var DbServer = require('./db/server');
var dbServer = new DbServer();
var WebServer = require('./webserver/server');
var webServer = new WebServer();

var EsServer = require('./es/server');
var esServer = new EsServer();

var util = require('util');
var events = require('events');

var JocondeLabAPI;

exports.JocondeLabAPI = JocondeLabAPI = function() {
	this.pubsub = new events.EventEmitter();
};

JocondeLabAPI.prototype.start = function ()	{
	dbServer.start(this.pubsub);
	webServer.start(this.pubsub);
	esServer.start(this.pubsub);

	console.log('started');
};

var api = new JocondeLabAPI();
api.start();