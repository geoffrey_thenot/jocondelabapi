'use strict';
var _ = require('underscore');

var elasticsearch = require('elasticsearch');

var Notice = function (server) {
	this.config = {
	    _index : 'joconde',
	    _type : 'notice',
	    server : server
	};
	this.es = elasticsearch(this.config);
};

Notice.prototype.get = function (defered, id) {
	this.es.get({
		_index:this.config._index, 
		_type: this.config._type, 
		_id: id}, function (err, data) {
			defered.resolve(data);
		}
	);
};

Notice.prototype.getByIds = function (defered, arrayIds) {
	var docs = _.map(arrayIds, function(value) { return {_id: value }; });
	this.es.multiGet({
		_index:this.config._index, 
		_type: this.config._type}, docs, function (err, data) {
			console.log(err, data);
			defered.resolve(data);
		}
	);
};

Notice.prototype.getSuggest = function (defered, terms) {
	var termFormated = _.map(terms, function (value) {
		return {"term": {"notice.term.name": value}};
	});

	this.es.search({
		"query": {
			"bool": {
				"should": termFormated
			}
		} ,
		"size" : 30,
	}, function (err, data) {
		defered.resolve(data.hits);
	});
};

Notice.prototype.search = function (defered, val, limit, filter) {
	var page = filter.p ? filter.p : 1;
	this.es.search({"query": {
		    "filtered": {
		    	"query" : {
		      		"query_string": {
		      			"default_field" : "notice.term.name",
	          			"query": val
	        		},
			 
		      	}
		    }
	  	},
	  	"from" : page * 30,
    	"size" : 30
	}, function (err, data) {
		defered.resolve(data);
	});
};


module.exports = Notice;