'use strict';

var Notice = require("./notice");
var Search = require("./search");
var esServer;

esServer = function () {
	this.es = null;
	this.server = {
	    	host : 'localhost',
	    	port : 9200
	  	};
};

esServer.prototype.start = function (pubsub)	{
	var notice = new Notice(this.server);
	var search = new Search(this.server);

	this.pubsub = pubsub;

	/* GET SUGGEST */
	this.pubsub.on('autocomplete', function (val, defered, limit, precise, filter) {
		search.autocomplete(defered, val, limit, precise, filter);
	}.bind(this));

	/* GET SEARCH RESULTS */
	this.pubsub.on('search', function (val, defered, limit, filter) {
		notice.search(defered, val, limit, filter);
	}.bind(this));

	/* GET NOTICE */
	this.pubsub.on('notice', function (defered, id) {
		notice.get(defered, id);
	}.bind(this));

	this.pubsub.on('notices', function (defered, ids) {
		notice.getByIds(defered, ids);
	}.bind(this));

	/* GET NOTICES */
	this.pubsub.on('favoritedNotices', function (defered, arrayId) {
		notice.getByIds(defered, arrayId);
	}.bind(this));

	/* GET SUGGEST NOTICE */
	this.pubsub.on('noticeSuggest', function (defered, terms) {
		notice.getSuggest(defered, terms);
	}.bind(this));
};

module.exports = esServer;