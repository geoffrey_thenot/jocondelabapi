'use strict';

var elasticsearch = require('elasticsearch');

var Search = function (server) {
	this.config = {
	    _index : 'search',
	    _type : 'autocomplete',
	    server : server
	};
	this.es = elasticsearch(this.config);
};

Search.prototype.autocomplete = function (defered, val, limit, precise, filter) {
	var field = "term.label";
	/*switch (filter) {
		case "location": field = "notice.loca"; break;
		case "size": field = "notice.dims"; break;
		case "technique": field = "notice.tech"; break;
		case "label": field = "notice.label"; break;
		case "source": field = "notice.srep"; break;
		case "date": field = "notice.mill"; break;
		case "periode": field = "notice.peri"; break;
		case "denomination": field = "notice.deno"; break;
		case "titre": field = "notice.titr"; break;
	}*/

	this.es.search({"query": {
		    "filtered": {
		    	"query" : {
		      		"query_string": {
		      			"default_field" : field,
	          			"query": val + precise  
	        		},
			 
		      	}
		    }
	  	},
    	/*"facets" : {
			"loca" : { "terms" : {"field" : "notice.loca", "size" : 5} },
			"dim" : { "terms" : {"field" : "notice.dims", "size" : 5} },
			"tech" : { "terms" : {"field" : "notice.tech", "size" : 5} },
			"label" : { "terms" : {"field" : "notice.label", "size" : 5} },
			"srep" : { "terms" : {"field" : "notice.srep", "size" : 5} },
			"mill" : { "terms" : {"field" : "notice.mill", "size" : 5} },
			"peri" : { "terms" : {"field" : "notice.peri", "size" : 5} },
			"deno" : { "terms" : {"field" : "notice.deno", "size" : 5} },
			"titr" : { "terms" : {"field" : "notice.titr", "size" : 5} }
	    },*/
		"from" : 0,
		"size" : limit,
		"sort" : ["_score"],
		"fields" : ["_source.term.label"],
	}, function (err, data) {
		defered.resolve(data);
	});
};

module.exports = Search;