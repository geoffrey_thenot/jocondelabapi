#!/bin/sh

if [ $# -ne 3 ]; then
    echo Usage: $0 url user password 1>&2
    echo -n "Voulez vous poursuivre avec les paramètres par défaut? (y/n)"
    read response
    if [ "$response" = "y" ] || [ "$response" = "Y" ]; then
    	url="jdbc:mysql://localhost:3306/jocondelab"
    	user="root"
    	password="root"
    else
    	echo Abandon
    	exit 1
    fi
else
	url=$1
	user=$2
	password=$3
fi

curl -XDELETE 'localhost:9200/joconde'

curl -XPUT 'localhost:9200/_river/my_jdbc_river/_meta' -d '{
    "type" : "jdbc",
    "jdbc" : {
        "driver" : "com.mysql.jdbc.Driver",
        "url" : "'$url'",
        "user" : "'$user'",
        "password" : "'$password'",
        "sql" : "select n.id as _id, n.repr as \"notice.repr\", n.mill as \"notice.mill\", n.pins as \"notice.pins\", n.titr as \"notice.titr\", n.autr as \"notice.autr\", n.paut as \"notice.paut\", n.gene as \"notice.gene\", n.aptn as \"notice.aptn\", n.attr as \"notice.attr\", n.srep as \"notice.srep\", n.tico as \"notice.tico\", n.peri as \"notice.peri\", n.hist as \"notice.hist\", n.deno as \"notice.deno\", n.loca as \"notice.loca\", n.drep as \"notice.drep\", n.dims as \"notice.dims\", n.tech as \"notice.tech\", n.dacq as \"notice.dacq\", t.label as \"term.name\", t.thesaurus_id as \"term.thesaurus_id\", i.relative_url as \"image.url\" FROM core_notice n JOIN core_noticeterm nt ON nt.notice_id = n.id JOIN core_term t ON t.id = nt.term_id LEFT JOIN core_noticeimage i ON i.notice_id = n.id"
    },
    "index" : {
        "index" : "joconde",
        "type" : "notice",

        "settings":{
            "analysis":{
              "analyzer":{
                "autocomplete":{
                  "type":"custom",
                  "tokenizer":"standard",
                  "filter":[ "standard", "lowercase", "stop", "kstem", "ngram" ] 
                }
              },
              "filter":{
                "ngram":{
                  "type":"ngram",
                  "min_gram":2,
                  "max_gram":15
                }
              }
            }
          }
    }
}'

# curl -XPUT 'localhost:9200/_river/my_jdbc_river/_meta' -d '{
#     "type" : "jdbc",
#     "jdbc" : {
#         "driver" : "com.mysql.jdbc.Driver",
#         "url" : "'$url'",
#         "user" : "'$user'",
#         "password" : "'$password'",
#         "sql" : "select t.id as _id, t.label as \"term.label\", t.lang as \"term.lang\", t.uri as \"term.uri\", t.dbpedia_uri as \"term.dbpedia_uri\", th.id as \"term.thesaurus_id\", th.label as \"term.thesaurus_label\" FROM core_term t LEFT JOIN core_thesaurus th ON t.thesaurus_id = th.id LIMIT 1000"
#     },
#     "index" : {
#         "index" : "joconde",
#         "type" : "term",

#         "settings":{
#             "analysis":{
#               "analyzer":{
#                 "autocomplete":{
#                   "type":"custom",
#                   "tokenizer":"standard",
#                   "filter":[ "standard", "lowercase", "stop", "kstem", "ngram" ] 
#                 }
#               },
#               "filter":{
#                 "ngram":{
#                   "type":"ngram",
#                   "min_gram":2,
#                   "max_gram":15
#                 }
#               }
#             }
#           }
#     }
# }'


# curl -XPUT 'localhost:9200/_river/my_jdbc_river/_meta' -d '{
#     "type" : "jdbc",
#     "jdbc" : {
#         "driver" : "com.mysql.jdbc.Driver",
#         "url" : "'$url'",
#         "user" : "'$user'",
#         "password" : "'$password'",
#         "sql" : "select t.id as _id, t.label as \"term.label\", t.lang as \"term.lang\", t.uri as \"term.uri\", t.dbpedia_uri as \"term.dbpedia_uri\", th.id as \"term.thesaurus_id\", th.label as \"term.thesaurus_label\", nt.notice_id as \"notice.id\" FROM core_term t LEFT JOIN core_thesaurus th ON t.thesaurus_id = th.id LEFT JOIN core_noticeterm nt ON nt.term_id = t.id LIMIT 100000"
#     },
#     "index" : {
#         "index" : "joconde",
#         "type" : "term_notice"
#     }
# }'


exit 0


# curl -XPOST 'localhost:9200/joconde/_search' -d  '{"query":{
#  "filtered": {
#     "query" : {
#       "query_string": {
#         "default_field" : "notice.titr",
#         "query": "vase"
#       }
#     }
#   }
# },
# "from": 0,
# "sort" : ["_score"],
# "fields" : ["_source.notice.titr"]
# }'

# curl -XPOST 'localhost:9001/oauth/token' -d '{
#   "payload": {
#     "username" : "geoffrey@gmail.com",
#     "password": "gg",
#     "grant_type": "password"
#   }
# }'