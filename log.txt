CREATE TABLE `users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `email` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `createdAt` DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP ,
  `updatedAt` DATETIME NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB;



CREATE TABLE `jocondelab`.`tokens` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `user_id` INT(11) NULL,
  `token` VARCHAR(255) NULL,
  PRIMARY KEY (`id`));


CREATE TABLE `jocondelab`.`favorites` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `user_id` INT NOT NULL,
  `notice_id` INT NOT NULL,
  PRIMARY KEY (`id`));


CREATE TABLE `echos` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) NOT NULL,
  `intro` TEXT NOT NULL,
  `content` TEXT NOT NULL,
  `about` TEXT NOT NULL,
  `image` varchar(255) NOT NULL,
  `createdAt` DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP ,
  `updatedAt` DATETIME NOT NULL,
  PRIMARY KEY (`id`)
);
ALTER TABLE `jocondelab`.`echos` CHARACTER SET = utf8 , COLLATE = utf8_general_ci , ENGINE = MyISAM ;


CREATE TABLE `echos_notices` (
  `echo_id` int(11) NOT NULL ,
  `notice_id` int(11) NOT NULL ,
  PRIMARY KEY (`echo_id`, `notice_id`)
);
