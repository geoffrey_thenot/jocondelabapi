Api joconde lab
===============

## Documentation

### Install
- `git clone https://bitbucket.org/geoffrey_thenot/jocondelabapi.git`
- install dependencies : 'npm install'
- import last dumps in jocondelab database
- run node
- start elasticsearch
- `sh run_es.sh`
- Restart elasticsearch
- `sh run_es2.sh`
- Restart elasticsearch

## Required
- nodejs
- mysql (default port 3306)
- elasticsearch

## Help

- How install elasticsearch ?
-- https://gist.github.com/Geoffrey-T/7316586
