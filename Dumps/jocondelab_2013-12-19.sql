# ************************************************************
# Sequel Pro SQL dump
# Version 4096
#
# http://www.sequelpro.com/
# http://code.google.com/p/sequel-pro/
#
# Hôte: 127.0.0.1 (MySQL 5.5.29)
# Base de données: jocondelab
# Temps de génération: 2013-12-19 18:53:41 +0000
# ************************************************************


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;


# Affichage de la table echos
# ------------------------------------------------------------

DROP TABLE IF EXISTS `echos`;

CREATE TABLE `echos` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `number` int(11) NOT NULL,
  `title` varchar(255) CHARACTER SET latin1 NOT NULL,
  `subject` varchar(255) NOT NULL,
  `subject_more` varchar(255) NOT NULL,
  `intro` text CHARACTER SET latin1 NOT NULL,
  `content` text CHARACTER SET latin1 NOT NULL,
  `about_title` varchar(255) NOT NULL DEFAULT '',
  `about` text CHARACTER SET latin1 NOT NULL,
  `image` varchar(255) CHARACTER SET latin1 NOT NULL,
  `lang` varchar(45) NOT NULL,
  `createdAt` datetime NOT NULL,
  `updatedAt` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `echos` WRITE;
/*!40000 ALTER TABLE `echos` DISABLE KEYS */;

INSERT INTO `echos` (`id`, `number`, `title`, `subject`, `subject_more`, `intro`, `content`, `about_title`, `about`, `image`, `lang`, `createdAt`, `updatedAt`)
VALUES
	(4,4,'Apollon et la réprésentation des corps au fil des siècles','Apollon','et la réprésentation des corps au fil des siècles','Apollon (en grec ancien ??????? / Apóllôn, en latin Apollo) est le dieu grec du chant, de la musique et de la poésie. Il est également dieu des purifications et de la guérison, mais peut apporter la peste avec son arc ; enfin, c\'est un des principaux dieux capables de divination, consulté, entre autres, à Delphes, où il rendait ses oracles par la Pythie. Il a aussi été honoré par les Romains, qui l\'ont adopté très rapidement sans changer son nom. \n\nDès le Ve siècle av. J.-C., ils l\'adoptèrent pour ses pouvoirs guérisseurs et lui élevèrent des temples.','<h2>apollon, un symbole mythologique</h2>\n\nAprès avoir réclamé l\'arc et la lyre, Apollon, dans l\'hymne homérique qui lui est consacré, nomme son troisième domaine d\'intervention : « je révélerai aussi dans mes oracles les desseins infaillibles de Zeus. » Si Zeus et quelques héros, comme Trophonios, possèdent leurs oracles, Apollon est la principale divinité oraculaire des Grecs. \nIl le déclare lui-même quand son frère Hermès essaie d\'obtenir aussi le don de divination : « j\'ai engagé ma parole, et juré par un serment redoutable que nul autre que moi, parmi les Dieux toujours vivants, ne connaîtrait la volonté de Zeus aux desseins profonds.»\nÀ partir de l\'époque classique, tous les sites oraculaires de grande envergure appartiennent à Apollon, à l\'exception de l\'oracle de Zeus à Dodone et, plus tard, de celui de Zeus Ammon à Siwa. Interrogé sur la disparition des oracles liés aux sources sacrées ou aux vapeurs émanant de la terre, Apollon répond au iie???iiie siècle ap. J.-C\nLe principal oracle d\'Apollon est celui de Delphes, qui est probablement fondé entre 900 et 700 av. J.-C.93 Dès l\'époque archaïque, Apollon delphien est omniprésent dans la vie des cités : il approuve leurs lois.\n\n<h2>une étude universelle</h2>\n\nLa musique est l\'art consistant à arranger et à ordonner ou désordonner sons et silences au cours du temps : le rythme est le support de cette combinaison dans le temps, la hauteur, celle de la combinaison dans les fréquences…\nElle est donc à la fois une création (une œuvre d\'art), une représentation et aussi un mode de communication. Elle utilise certaines règles ou systèmes de composition, des plus simples aux plus complexes (souvent les notes de musique, les gammes et autres). Elle peut utiliser des objets divers, le corps, la voix, mais aussi des instruments de musique spécialement conçus, et de plus en plus tous les sons (concrets, de synthèses, abstraits, etc.).\nL\'histoire de la musique est une matière particulièrement riche et complexe du fait principalement de ses caractéristiques: la difficulté tient d\'abord à l\'ancienneté de la musique, phénomène universel remontant à la Préhistoire, qui a donné lieu à la formation de traditions qui se sont développées séparément à travers le monde sur des millénaires.\nIl y a donc une multitude de très longues Histoires de la musique selon les cultures et civilisations. La musique occidentale (musique classique ou pop-rock au sens très large) prenant qu\'au xvie siècle l\'allure de référence internationale, et encore très partiellement.\n\n<h2>la renaissance</h2> \n\nSelon Jean Delumeau, spécialiste de la Renaissance, le mot Renaissance nous est venu d\'Italie et des arts dès la fin du xive siècle (les Italiens disent aujourd\'hui Rinascimento).\nLe terme de « Renaissance » en tant qu\'époque et non plus pour désigner un renouveau des lettres et des arts, a été utilisé pour la première fois en 1840 par Jean-Jacques Ampère dans son Histoire littéraire de la France avant le xiie siècle2puis par Jules Michelet en 1855 dans son volume consacré au xvie siècle La Renaissance dans le cadre de son Histoire de France. Ce terme a été repris en 1860 par l\'historien de l\'art suisse Jacob Burckhardt (1818-1897) dans son livre Culture de la Renaissance en Italie.\nDans son cours au Collège de France en 1942-43, l\'historien français Lucien Febvre montre que Jules Michelet a utilisé ce terme pour des raisons personnelles. En effet, Jules Michelet, travaillant sur le roi Louis XI alors qu\'il était attristé par la perte de son épouse et contrarié par l\'évolution politique conservatrice de la Monarchie de juillet, eut un besoin profond de nouveauté, de renouvellement.\n','Philippe Marquès','Après que la seconde guerre mondiale ai pris fin, Philippe Marquès se lance dans le journalisme et en particulier dans le journalisme d\'art. \nIl est le créateur de ce nouveau format dans les années 60.\nMais aussi d\'autres choses très intéressantes, telles que l\'application de ce format à l\'université où il a passé la seconde moitié de sa carrière.','/images/echos/apollon.jpg','fr','2013-12-08 18:43:26','0000-00-00 00:00:00'),
	(6,4,'Apollo and bodies’s representation over the centuries','Apollo','and bodies\'s representation over the centuries','Apollo (Greek ??????? / Apollo, Apollo in Latin) is the Greek god of song, music and poetry. It is also the god of purification and healing, but can bring the plague with his bow, and finally, this is one of the main gods capable of divination consulted, among others, at Delphi, where he made ??his oracles by the Pythia. He was also honored by the Romans, who adopted very quickly without changing its name.\nFrom the fifth century BC. BC, they adopted it for its healing powers and he raised temples.\n','<h2>Apollo , a mythological symbol</h2>\n\nAfter claiming the bow and the lyre , Apollo in the Homeric hymn dedicated to him , called his third area of intervention: \" I also reveal in my infallible oracles designs of Zeus. \" If Zeus and some heroes, like Trophonios , have their oracles , Apollo is the main oracular deity of the Greeks.\nHe declares himself when his brother Hermes tries to get as the gift of divination: \"I have pledged my word, and sworn by a formidable oath that no one but me , still alive among the gods only know the will Zeus the deep designs\"\nFrom the classical period, all oracular large sites belong to Apollo, with the exception of the oracle of Zeus at Dodona and , later, that of Zeus Ammon at Siwa. \nAsked about the disappearance of the oracles associated with sacred springs or mist from the earth, Apollon meets the iie - third century AD. J. -C\nThe principal is the oracle of Apollo at Delphi , which is probably founded between 900 and 700 BC. J. C.93 From the Archaic period, Delphian Apollo is ubiquitous in the life of cities : it approves laws .\n\n<h2>an universal study</h2>\n\nMusic is the art of arranging and ordering or disordering sounds and silences in time : rhythm is the support of this combination in time, the height , the combination frequencies in ... It is therefore both a creation ( an artwork ) representation and also a mode of communication. \nIt uses certain rules or composition systems, from the simplest to the most complex (often musical notes , scales, and others). It can use various objects, the body, the voice, but also musical instruments specially designed and more all sounds(concrete, synthesis, abstract , etc.). .\nThe history of music is a rich and complex material mainly because of its characteristics: the first challenge is to seniority music, universal phenomenon dating back to prehistoric times, which led to the formation of traditions that have developed separately worldwide millennia .\nSo there is a multitude of very long stories of music in different cultures and civilizations. Western music ( classical or pop -rock in a very broad sense) taking in the sixteenth century the pace of international reference, and even partially.\n\n<h2>renaissance</h2>\n\nAccording to Jean Delumeau specialist of the Renaissance, the word came to us of Renaissance Italy and the arts from the late fourteenth century ( the Italians say today Rinascimento ) .\nThe term \"Renaissance\" as the epoch and not to refer to a revival of arts and letters , was used for the first time in 1840 by Jean- Jacques Ampere in his Literary History of France before the twelfth siècle2puis by Jules Michelet in 1855 in his volume on the sixteenth century Renaissance as part of his History of France . This term was taken in 1860 by the Swiss art historian Jacob Burckhardt ( 1818-1897 ) in his book Culture of Renaissance Italy.\nIn his lecture at the Collège de France in 1942-43 , French historian Lucien Febvre shows that Jules Michelet used this term for personal reasons . Indeed, Jules Michelet , working on King Louis XI while he was saddened by the loss of his wife and thwarted by the conservative political evolution of the July Monarchy , had a deep need for new , renewal .\n','Philippe Marquès','After World War I ended, Philippe Marquès launches in journalism and especially in the art of journalism.\nHe is the creator of the new format in the 60s.\nBut also other very interesting things, such as the application of this format at the university where he spent the second half of his career.','/images/echos/apollon.jpg','en','2013-12-08 18:43:28','0000-00-00 00:00:00'),
	(12,2,'Calder et la géométrie','Calder','et la géométrie','Alexander Calder est un sculpteur et peintre américain né le 22 juillet 1898 à Lawnton près de Philadelphie et mort le 11 novembre 1976 à New York. \n\nIl est surtout connu pour ses mobiles, assemblages de formes animés par les mouvements de l\'air, et ses stabiles, « la sublimation d\'un arbre dans le vent » d\'après Marcel Duchamp.','<h2>une technique personnelle </h2>\n\nEn 1925, il réalise sur commande l\'illustration des spectacles du cirque Ringling Bros. and Barnum & Bailey Circus. \nIl va découvrir une fascination pour le thème du cirque qui débouchera sur son Cirque de Calder, une performance où interviennent des figures faites de fil de fer et dans laquelle l\'artiste joue le rôle de maître de cérémonie, de chef de piste et de marionnettiste en faisant fonctionner manuellement le mécanisme, le tout étant accompagné de musique et d\'effets sonores. Le Cirque de Calder se produira à Paris en 1926.\nIl s\'installe en France en 1927, où il fabrique des jouets et donne des représentations avec son cirque de marionnettes, en fil de fer ainsi qu\'en bois articulés. Il entre en contact avec des représentants de l\'avant-garde artistique parisienne comme Joan Miró, Jean Cocteau, Man Ray, Robert Desnos, Fernand Léger, Le Corbusier, Theo van Doesburg et Piet Mondrian en 1930 qui aura une grande influence artistique sur lui. Il abandonne la sculpture figurative en fil de fer qu\'il avait pratiquée depuis 1926 pour adopter un langage sculptural entièrement abstrait.En 1943, le Museum of Modern Art organise une première rétrospective.\n\n<h2>Une étude universelle</h2>\n\nLa musique est l\'art consistant à arranger et à ordonner ou désordonner sons et silences au cours du temps : le rythme est le support de cette combinaison dans le temps, la hauteur, celle de la combinaison dans les fréquences…\nElle est donc à la fois une création (une œuvre d\'art), une représentation et aussi un mode de communication. Elle utilise certaines règles ou systèmes de composition, des plus simples aux plus complexes (souvent les notes de musique, les gammes et autres). Elle peut utiliser des objets divers, le corps, la voix, mais aussi des instruments de musique spécialement conçus, et de plus en plus tous les sons (concrets, de synthèses, abstraits, etc.).\nL\'histoire de la musique est une matière particulièrement riche et complexe du fait principalement de ses caractéristiques: la difficulté tient d\'abord à l\'ancienneté de la musique, phénomène universel remontant à la Préhistoire, qui a donné lieu à la formation de traditions qui se sont développées séparément à travers le monde sur des millénaires.\nIl y a donc une multitude de très longues Histoires de la musique selon les cultures et civilisations. La musique occidentale (musique classique ou pop-rock au sens très large) ne prenant qu\'au xvie siècle l\'allure de référence internationale, et encore très partiellement.\n\n<h2>la renaissance</h2> \n\nSelon Jean Delumeau, spécialiste de la Renaissance, le mot Renaissance nous est venu d\'Italie et des arts dès la fin du xive siècle (les Italiens disent aujourd\'hui Rinascimento).\nLe terme de « Renaissance » en tant qu\'époque et non plus pour désigner un renouveau des lettres et des arts, a été utilisé pour la première fois en 1840 par Jean-Jacques Ampère dans son Histoire littéraire de la France avant le xiie siècle2puis par Jules Michelet en 1855 dans son volume consacré au xvie siècle La Renaissance dans le cadre de son Histoire de France. Ce terme a été repris en 1860 par l\'historien de l\'art suisse Jacob Burckhardt (1818-1897) dans son livre Culture de la Renaissance en Italie.\nDans son cours au Collège de France en 1942-43, l\'historien français Lucien Febvre montre que Jules Michelet a utilisé ce terme pour des raisons personnelles. En effet, Jules Michelet, travaillant sur le roi Louis XI alors qu\'il était attristé par la perte de son épouse et contrarié par l\'évolution politique conservatrice de la Monarchie de juillet, eut un besoin profond de nouveauté, de renouvellement.','Antoine d’Antana','Après que la seconde guerre mondiale ai pris fin, A. d’Antana se lance dans le journalisme et en particulier dans le journalisme d\'art. \nIl est le créateur de ce nouveau format dans les années 60.\nMais aussi d\'autres choses très intéressantes, telles que l\'application de ce format à l\'université où il a passé la seconde moitié de sa carrière.\n','/images/echos/calder.jpg','fr','0000-00-00 00:00:00','0000-00-00 00:00:00'),
	(13,2,'Calder and geometry','Calder','and geometry','Alexander Calder was an American sculptor and painter born July 22, 1898 at Lawnton around Philadelphia and died November 11, 1976 in New York.\n\nHe is best known for its mobile, assembled from pieces by the movement of air, and its stabile, \"the sublimation of a tree in the wind\" by Marcel Duchamp.','<h2>a personal technique</h2>\n\nIn 1925, he creates custom illustration shows Ringling Bros. circus . and Barnum & Bailey Circus.\nHe discovers a fascination with circus theme that will lead to his Cirque Calder , a performance which figures made of wire , in which the artist plays the role of master of ceremonies, Ringmaster and puppeteer involved by operating the mechanism manually , all of which are accompanied by music and sound effects. \nCirque Calder happen to Paris in 1926. He moved to France in 1927 , where it manufactures toys and gives performances with his puppet circus , wire and articulated in wood. \nIt comes in contact with representatives of the artistic avant-garde Parisian as Joan Miró , Jean Cocteau, Man Ray , Robert Desnos , Fernand Léger , Le Corbusier, Theo van Doesburg and Piet Mondrian in 1930 which will have a great influence on her artistic . He abandoned figurative sculpture wire he had since 1926, to adopt a sculptural language entirely abstrait.En 1943, the Museum of Modern Art organized the first retrospective .\n\n<h2>universal study</h2>\n\nMusic is the art of arranging and ordering or disordering sounds and silences in time : rhythm is the support of this combination in time, the height , the combination frequencies in ...\nIt is therefore both a creation ( an artwork ) representation and also a mode of communication. It uses certain rules or composition systems , from the simplest to the most complex (often musical notes , scales, and others). \nIt can use various objects, the body, the voice, but also musical instruments specially designed and more all sounds (concrete, synthesis, abstract , etc.). .\nThe history of music is a rich and complex material mainly because of its characteristics : the first challenge is to seniority music, universal phenomenon dating back to prehistoric times, which led to the formation of traditions that have developed separately worldwide millennia .\nSo there is a multitude of very long stories of music in different cultures and civilizations. Western music ( classical or pop -rock in a very broad sense) taking in the sixteenth century the pace of international reference, and even partially.\n\n<h2>renaissance</h2>\n\nAccording to Jean Delumeau specialist of the Renaissance, the word came to us of Renaissance Italy and the arts from the late fourteenth century ( the Italians say today Rinascimento ) .\nThe term \"Renaissance\" as the epoch and not to refer to a revival of arts and letters , was used for the first time in 1840 by Jean- Jacques Ampere in his Literary History of France before the twelfth siècle2puis by Jules Michelet in 1855 in his volume on the sixteenth century Renaissance as part of his History of France . This term was taken in 1860 by the Swiss art historian Jacob Burckhardt ( 1818-1897 ) in his book Culture of Renaissance Italy.\nIn his lecture at the Collège de France in 1942-43 , French historian Lucien Febvre shows that Jules Michelet used this term for personal reasons . Indeed, Jules Michelet , working on King Louis XI while he was saddened by the loss of his wife and thwarted by the conservative political evolution of the July Monarchy , had a deep need for new, renewal.','Antoine d’Antana','After World War I ended, A. Antana of launches in journalism and in particular in the art journalism.\nHe is the creator of the new format in the 60s.\nBut also other very interesting things, such as the application of this format at the university where he spent the second half of his career.','/images/echos/calder.jpg','en','0000-00-00 00:00:00','0000-00-00 00:00:00'),
	(14,3,'Manet et l’impressionnisme dans l’art du XXe siècle','Édouart Manet','et l’impressionnisme dans l’art du XXe siècle','Après la guerre franco-prussienne de 1870 à laquelle il participe, Manet soutient les Impressionnistes parmi lesquels il a des amis proches comme Claude Monet, Auguste Renoir ou Berthe Morisot qui devient sa belle-sœur et dont sera remarqué le célèbre portrait, parmi ceux qu\'il fera d\'elle, Berthe Morisot au bouquet de violettes (1872).\n <br />\nÀ leur contact, il délaisse en partie la peinture d\'atelier pour la peinture en plein air à Argenteuil et Gennevilliers, où il possède une maison. Il réalisa dans cette dernière, bon nombre de ses plus belles œuvres.\n','<h2>L’arrivée du rouge dans sa palette </h2>\n\nManet obtient des résultats convenables au collège Rollin, bien que les études ne l\'intéressent pas. Mais il refuse de s\'inscrire à la faculté de droit malgré les pressions de son père, et il demande à entrer dans la marine après de pénibles débats en famille. Mais il échoue au concours du Borda.\nEn 1849, Manet se représente au concours du Borda et il échoue de nouveau, mais il revient avec une multitude de dessins dans ses bagages devant lesquels son père se rend à l\'évidence : Édouard est un artiste.\nIl peint aussi des natures mortes, souvent réalisées pour des raisons financières (Manet parvenait plus facilement à les écouler que ses portraits), elles n\'en montrent pas moins le grand art du peintre, qui parvient à représenter fleurs, fruits et légumes dans une véritable mise en scène dramatique. Manet effectue aussi des portraits de femmes, (Nana, 1877, Femme blonde avec seins nus, 1878) ou de ses familiers comme le poète Stéphane Mallarmé en 1876 ou Georges Clemenceau en 1879-1880. Il est alors de plus en plus reconnu et reçoit la Légion d’honneur le 1er janvier 1882. Et plein d’autre choses en fait, c’est juste pour essayer de finir mon paragraphe dans ma grille. \n\n<h2>relation picturale</h2>\n\nIl peint aussi des natures mortes, souvent réalisées pour des raisons financières (Manet parvenait plus facilement à les écouler, que son ensemble de portraits), elles n\'en montrent pas moins le grand art du peintre, qui parvient à représenter des fleurs, des fruits et légumes dans une véritable mise en scène dramatique. \nManet effectue aussi des portraits de femmes, ou de ses familiers comme le poète Stéphane Mallarmé en 1876 ou Georges Clemenceau en 1879-1880.\nMais il refuse de s\'inscrire à la faculté de droit malgré les pressions de son père, et il demande à entrer dans la marine après de pénibles débats en famille. Mais il échoue au concours du Borda.\nEn 1849, Manet se représente au concours du Borda et il échoue de nouveau, mais il revient avec une multitude de dessins dans ses bagages devant lesquels son père se rend à l\'évidence : Édouard est un artiste.\nIl peint aussi des natures mortes, souvent réalisées pour des raisons financières (Manet parvenait plus facilement à les écouler, que son ensemble de portraits), elles n\'en montrent pas moins le grand art du peintre, qui parvient à représenter des fleurs, des fruits et légumes dans une véritable mise en scène dramatique.\nManet effectue aussi des portraits de femmes, ou de ses familiers comme le poète Stéphane Mallarmé en 1876 ou Georges Clemenceau en 1879-1880.\n\n<h2>L’impressionnisme</h2>\n\nParmi les trois peintures exposées au Salon, la composition centrale du Déjeuner sur l’herbe suscite les réactions les plus vives. Dans cette œuvre, Manet y confirme sa rupture avec le classicisme ainsi que l’académisme qu\'il avait commencée avec «La Musique aux Tuileries». La polémique vient moins du style de la toile que de son sujet.\nUne histoire de femmes nues en compagnie d’homme habillés de façon très classique ce qui véritablement fait débat.\nIl peint aussi des natures mortes, souvent réalisées pour des raisons financières (Manet parvenait plus facilement à les écouler que ses portraits), elles n\'en montrent pas moins le grand art du peintre, qui parvient à représenter fleurs, fruits et légumes dans une véritable mise en scène dramatique. Manet effectue aussi des portraits de femmes, (Nana, 1877, Femme blonde avec seins nus, 1878) ou de ses familiers comme le poète Stéphane Mallarmé.\n','Marc Leflocon','Après que la seconde guerre mondiale ai pris fin, Marc Leflocon se lance dans le journalisme et en particulier dans le journalisme d\'art. \nIl est le créateur de ce nouveau format dans les années 60.\nMais aussi d\'autres choses très intéressantes, telles que l\'application de ce format à l\'université où il a passé la seconde moitié de sa carrière.','/images/echos/manet.jpg','fr','0000-00-00 00:00:00','0000-00-00 00:00:00'),
	(15,3,'Manet and Impressionism in twentieth-century art','Édouart Manet','and Impressionism in  twentieth-century art','After the Franco-Prussian war of 1870 in which it participates, supports the Impressionists Manet among whom he has close friends like Claude Monet, Auguste Renoir and Berthe Morisot who became his sister-in which will be noticed the famous portrait, among those that it will make her Berthe Morisot with a bouquet of violets.\n<br /><br />\nUpon contact, he left in part workshop for painting outdoors in Gennevilliers and Argenteuil, where he owns a house painting. He realized in the latter, many of his finest works.','<h2>The arrival of the red in the palette</h2>\n\nManet get suitable results Rollin college, although studies do not interest him . But he refuses to enroll in law school despite pressure from his father, and asked to join the Navy after arduous discussions family. But it fails to contest Borda .\nIn 1849, Manet is the contest of Borda and it fails again, but he returned with a multitude of drawings in his luggage before whom his father went to the obvious: Edward is an artist.\nHe also painted still lifes , often made for financial reasons (Manet succeeded easier to sell his portraits) , they do not show under the great art of the painter , who manages to represent flowers, fruits and vegetables in a real dramatic setting . \nManet also carries portraits of women ( Nana , 1877 Blond Woman with Bare Breasts, 1878 ) or their relatives as the poet Stéphane Mallarmé in 1876 and Georges Clemenceau 1879-1880 . \nThen it is increasingly recognized and received the Legion of Honor on 1 January 1882. And many other things in fact, it is just trying to finish my paragraph in my grid.\n\n<h2>pictorial relationship</h2>\n\nHe also painted still lifes , often made for financial reasons (Manet managed more easily to pass , that a whole of portraits) , they do not show under the great art of the painter , who manages to represent flowers, fruits and vegetables in a real dramatic setting .\nManet also carries portraits of women , or their relatives as the poet Stéphane Mallarmé in 1876 and Georges Clemenceau 1879-1880 .\nBut he refuses to enroll in law school despite pressure from his father, and asked to join the Navy after arduous discussions family. But it fails to contest Borda .\nIn 1849, Manet is the contest of Borda and it fails again, but he returned with a multitude of drawings in his luggage before whom his father went to the obvious: Edward is an artist.\nHe also painted still lifes , often made for financial reasons (Manet managed more easily to pass , that a whole of portraits) , they do not show under the great art of the painter , who manages to represent flowers, fruits and vegetables in a real dramatic setting.\nManet also carries portraits of women, or their relatives as the poet Stéphane Mallarmé in 1876 and Georges Clemenceau 1879-1880.\n\n<h2>Impressionism</h2>\n\nOf the three paintings exhibited at the Salon, the central composition of Déjeuner sur l\'herbe arouses the strongest reactions. In this work, Manet and confirmed his break with classicism and academicism he had begun with \"The Music in the Tuileries.\" The controversy comes less style canvas as its subject.\nA story of naked women in the company of classic dressed man so what really makes debate.\nHe also painted still lifes, often made for financial reasons (Manet succeeded easier to sell his portraits), they do not show under the great art of the painter, who manages to represent flowers, fruits and vegetables in a real dramatic setting. Manet also carries portraits of women (Nana, 1877 Blond Woman with Bare Breasts, 1878) or their relatives as the poet Stéphane Mallarmé.\n','Marc Leflocon','After World War I ended, Marc Leflocon launches in journalism and especially in the art of journalism.\nHe is the creator of the new format in the 60s.\nBut also other very interesting things, such as the application of this format at the university where he spent the second half of his career.','/images/echos/manet.jpg','en','0000-00-00 00:00:00','0000-00-00 00:00:00');

/*!40000 ALTER TABLE `echos` ENABLE KEYS */;
UNLOCK TABLES;


# Affichage de la table echos_notices
# ------------------------------------------------------------

DROP TABLE IF EXISTS `echos_notices`;

CREATE TABLE `echos_notices` (
  `echo_id` int(11) NOT NULL,
  `notice_id` int(11) NOT NULL,
  `createdAt` datetime NOT NULL,
  `updatedAt` datetime NOT NULL,
  PRIMARY KEY (`echo_id`,`notice_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

LOCK TABLES `echos_notices` WRITE;
/*!40000 ALTER TABLE `echos_notices` DISABLE KEYS */;

INSERT INTO `echos_notices` (`echo_id`, `notice_id`, `createdAt`, `updatedAt`)
VALUES
	(0,0,'0000-00-00 00:00:00','0000-00-00 00:00:00'),
	(1,1,'0000-00-00 00:00:00','0000-00-00 00:00:00'),
	(1,2,'0000-00-00 00:00:00','0000-00-00 00:00:00'),
	(14,57465,'0000-00-00 00:00:00','0000-00-00 00:00:00'),
	(14,57472,'0000-00-00 00:00:00','0000-00-00 00:00:00'),
	(14,57477,'0000-00-00 00:00:00','0000-00-00 00:00:00'),
	(14,57484,'0000-00-00 00:00:00','0000-00-00 00:00:00'),
	(14,76016,'0000-00-00 00:00:00','0000-00-00 00:00:00'),
	(15,57465,'0000-00-00 00:00:00','0000-00-00 00:00:00'),
	(15,57472,'0000-00-00 00:00:00','0000-00-00 00:00:00'),
	(15,57477,'0000-00-00 00:00:00','0000-00-00 00:00:00'),
	(15,57484,'0000-00-00 00:00:00','0000-00-00 00:00:00'),
	(15,76016,'0000-00-00 00:00:00','0000-00-00 00:00:00');

/*!40000 ALTER TABLE `echos_notices` ENABLE KEYS */;
UNLOCK TABLES;


# Affichage de la table favorites
# ------------------------------------------------------------

DROP TABLE IF EXISTS `favorites`;

CREATE TABLE `favorites` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `object_id` int(11) NOT NULL,
  `type` int(2) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

LOCK TABLES `favorites` WRITE;
/*!40000 ALTER TABLE `favorites` DISABLE KEYS */;

INSERT INTO `favorites` (`id`, `user_id`, `object_id`, `type`)
VALUES
	(2,2,61172,1),
	(5,2,2,2),
	(9,2,57376,1),
	(11,2,1,2),
	(12,2,57483,1),
	(16,2,3,2);

/*!40000 ALTER TABLE `favorites` ENABLE KEYS */;
UNLOCK TABLES;


# Affichage de la table tokens
# ------------------------------------------------------------

DROP TABLE IF EXISTS `tokens`;

CREATE TABLE `tokens` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT NULL,
  `token` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

LOCK TABLES `tokens` WRITE;
/*!40000 ALTER TABLE `tokens` DISABLE KEYS */;

INSERT INTO `tokens` (`id`, `user_id`, `token`)
VALUES
	(3,2,'YZ966dAQBdbpRX9/d0OMb33wpDBrPRzuHfbULK1lOQU='),
	(4,2,'YZ966dAQBdbpRX9/d0OMb33wpDBrPRzuHfbULK1lOQU='),
	(5,2,'YZ966dAQBdbpRX9/d0OMb33wpDBrPRzuHfbULK1lOQU='),
	(6,2,'YZ966dAQBdbpRX9/d0OMb33wpDBrPRzuHfbULK1lOQU='),
	(7,2,'YZ966dAQBdbpRX9/d0OMb33wpDBrPRzuHfbULK1lOQU='),
	(8,2,'YZ966dAQBdbpRX9/d0OMb33wpDBrPRzuHfbULK1lOQU='),
	(9,2,'YZ966dAQBdbpRX9/d0OMb33wpDBrPRzuHfbULK1lOQU='),
	(10,2,'YZ966dAQBdbpRX9/d0OMb33wpDBrPRzuHfbULK1lOQU='),
	(11,2,'YZ966dAQBdbpRX9/d0OMb33wpDBrPRzuHfbULK1lOQU='),
	(12,2,'YZ966dAQBdbpRX9/d0OMb33wpDBrPRzuHfbULK1lOQU='),
	(13,2,'YZ966dAQBdbpRX9/d0OMb33wpDBrPRzuHfbULK1lOQU='),
	(14,2,'YZ966dAQBdbpRX9/d0OMb33wpDBrPRzuHfbULK1lOQU='),
	(15,2,'YZ966dAQBdbpRX9/d0OMb33wpDBrPRzuHfbULK1lOQU='),
	(16,2,'YZ966dAQBdbpRX9/d0OMb33wpDBrPRzuHfbULK1lOQU='),
	(17,2,'YZ966dAQBdbpRX9/d0OMb33wpDBrPRzuHfbULK1lOQU='),
	(18,2,'YZ966dAQBdbpRX9/d0OMb33wpDBrPRzuHfbULK1lOQU='),
	(19,2,'YZ966dAQBdbpRX9/d0OMb33wpDBrPRzuHfbULK1lOQU='),
	(20,2,'YZ966dAQBdbpRX9/d0OMb33wpDBrPRzuHfbULK1lOQU=');

/*!40000 ALTER TABLE `tokens` ENABLE KEYS */;
UNLOCK TABLES;


# Affichage de la table users
# ------------------------------------------------------------

DROP TABLE IF EXISTS `users`;

CREATE TABLE `users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `email` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `createdAt` datetime NOT NULL,
  `updatedAt` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;

INSERT INTO `users` (`id`, `email`, `password`, `createdAt`, `updatedAt`)
VALUES
	(2,'geoffrey@gmail.com','381d9ef64788ee4d21d61faed2e39c72d862b92d58689a9d9c991d38c6b4b315','2013-12-16 13:38:49','2013-12-16 13:38:49');

/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;



/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
