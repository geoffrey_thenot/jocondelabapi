CREATE DATABASE  IF NOT EXISTS `jocondelab` /*!40100 DEFAULT CHARACTER SET latin1 */;
USE `jocondelab`;
-- MySQL dump 10.14  Distrib 10.0.4-MariaDB, for debian-linux-gnu (x86_64)
--
-- Host: localhost    Database: jocondelab
-- ------------------------------------------------------
-- Server version	10.0.4-MariaDB-1~precise-log

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `echos`
--

DROP TABLE IF EXISTS `echos`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `echos` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) CHARACTER SET latin1 NOT NULL,
  `intro` text CHARACTER SET latin1 NOT NULL,
  `content` text CHARACTER SET latin1 NOT NULL,
  `about` text CHARACTER SET latin1 NOT NULL,
  `image` varchar(255) CHARACTER SET latin1 NOT NULL,
  `lang` varchar(45) NOT NULL,
  `createdAt` datetime NOT NULL,
  `updatedAt` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `echos`
--

LOCK TABLES `echos` WRITE;
/*!40000 ALTER TABLE `echos` DISABLE KEYS */;
INSERT INTO `echos` VALUES (1,'Alexander Calder et la géométrie','Alexander Calder est un sculpteur et peintre américain né le 22 juillet 1898 à Lawnton près de Philadelphie et mort le 11 novembre 1976 à New York. Il est surtout connu pour ses mobiles, assemblages de formes animés par les mouvements de l\'air, et ses stabiles, « la sublimation d\'un arbre dans le vent » d\'après Marcel Duchamp.','test','Après que la seconde guerre mondiale ai pris fin, Marc Leflocon se lance dans le journalisme et en particulier dans le journalisme d\'art. \nIl est le créateur de ce nouveau format dans les années 60.','','fr','2013-12-06 23:10:20','0000-00-00 00:00:00'),(2,'Alexander Calder et la géométrie','Alexander Calder est un sculpteur et peintre américain né le 22 juillet 1898 à Lawnton près de Philadelphie et mort le 11 novembre 1976 à New York. Il est surtout connu pour ses mobiles, assemblages de formes animés par les mouvements de l\'air, et ses stabiles, « la sublimation d\'un arbre dans le vent » d\'après Marcel Duchamp.','test','Après que la seconde guerre mondiale ai pris fin, Marc Leflocon se lance dans le journalisme et en particulier dans le journalisme d\'art. \r\nIl est le créateur de ce nouveau format dans les années 60.','','fr','2013-12-08 18:43:09','0000-00-00 00:00:00'),(3,'Alexander Calder et la géométrie','Alexander Calder est un sculpteur et peintre américain né le 22 juillet 1898 à Lawnton près de Philadelphie et mort le 11 novembre 1976 à New York. Il est surtout connu pour ses mobiles, assemblages de formes animés par les mouvements de l\'air, et ses stabiles, « la sublimation d\'un arbre dans le vent » d\'après Marcel Duchamp.','test','Après que la seconde guerre mondiale ai pris fin, Marc Leflocon se lance dans le journalisme et en particulier dans le journalisme d\'art. \r\nIl est le créateur de ce nouveau format dans les années 60.','','fr','2013-12-08 18:43:25','0000-00-00 00:00:00'),(4,'Alexander Calder et la géométrie','Alexander Calder est un sculpteur et peintre américain né le 22 juillet 1898 à Lawnton près de Philadelphie et mort le 11 novembre 1976 à New York. Il est surtout connu pour ses mobiles, assemblages de formes animés par les mouvements de l\'air, et ses stabiles, « la sublimation d\'un arbre dans le vent » d\'après Marcel Duchamp.','test','Après que la seconde guerre mondiale ai pris fin, Marc Leflocon se lance dans le journalisme et en particulier dans le journalisme d\'art. \r\nIl est le créateur de ce nouveau format dans les années 60.','','fr','2013-12-08 18:43:26','0000-00-00 00:00:00'),(5,'Alexander Calder et la géométrie','Alexander Calder est un sculpteur et peintre américain né le 22 juillet 1898 à Lawnton près de Philadelphie et mort le 11 novembre 1976 à New York. Il est surtout connu pour ses mobiles, assemblages de formes animés par les mouvements de l\'air, et ses stabiles, « la sublimation d\'un arbre dans le vent » d\'après Marcel Duchamp.','test','Après que la seconde guerre mondiale ai pris fin, Marc Leflocon se lance dans le journalisme et en particulier dans le journalisme d\'art. \r\nIl est le créateur de ce nouveau format dans les années 60.','','fr','2013-12-08 18:43:27','0000-00-00 00:00:00'),(6,'Alexander Calder et la géométrie','Alexander Calder est un sculpteur et peintre américain né le 22 juillet 1898 à Lawnton près de Philadelphie et mort le 11 novembre 1976 à New York. Il est surtout connu pour ses mobiles, assemblages de formes animés par les mouvements de l\'air, et ses stabiles, « la sublimation d\'un arbre dans le vent » d\'après Marcel Duchamp.','test','Après que la seconde guerre mondiale ai pris fin, Marc Leflocon se lance dans le journalisme et en particulier dans le journalisme d\'art. \r\nIl est le créateur de ce nouveau format dans les années 60.','','fr','2013-12-08 18:43:28','0000-00-00 00:00:00'),(7,'Alexander Calder et la géométrie','Alexander Calder est un sculpteur et peintre américain né le 22 juillet 1898 à Lawnton près de Philadelphie et mort le 11 novembre 1976 à New York. Il est surtout connu pour ses mobiles, assemblages de formes animés par les mouvements de l\'air, et ses stabiles, « la sublimation d\'un arbre dans le vent » d\'après Marcel Duchamp.','test','Après que la seconde guerre mondiale ai pris fin, Marc Leflocon se lance dans le journalisme et en particulier dans le journalisme d\'art. \r\nIl est le créateur de ce nouveau format dans les années 60.','','fr','2013-12-08 18:43:29','0000-00-00 00:00:00'),(8,'Alexander Calder et la géométrie','Alexander Calder est un sculpteur et peintre américain né le 22 juillet 1898 à Lawnton près de Philadelphie et mort le 11 novembre 1976 à New York. Il est surtout connu pour ses mobiles, assemblages de formes animés par les mouvements de l\'air, et ses stabiles, « la sublimation d\'un arbre dans le vent » d\'après Marcel Duchamp.','test','Après que la seconde guerre mondiale ai pris fin, Marc Leflocon se lance dans le journalisme et en particulier dans le journalisme d\'art. \r\nIl est le créateur de ce nouveau format dans les années 60.','','fr','2013-12-08 18:43:30','0000-00-00 00:00:00'),(9,'Alexander Calder et la géométrie','Alexander Calder est un sculpteur et peintre américain né le 22 juillet 1898 à Lawnton près de Philadelphie et mort le 11 novembre 1976 à New York. Il est surtout connu pour ses mobiles, assemblages de formes animés par les mouvements de l\'air, et ses stabiles, « la sublimation d\'un arbre dans le vent » d\'après Marcel Duchamp.','test','Après que la seconde guerre mondiale ai pris fin, Marc Leflocon se lance dans le journalisme et en particulier dans le journalisme d\'art. \r\nIl est le créateur de ce nouveau format dans les années 60.','','fr','2013-12-08 18:43:31','0000-00-00 00:00:00'),(10,'Alexander Calder et la géométrie','Alexander Calder est un sculpteur et peintre américain né le 22 juillet 1898 à Lawnton près de Philadelphie et mort le 11 novembre 1976 à New York. Il est surtout connu pour ses mobiles, assemblages de formes animés par les mouvements de l\'air, et ses stabiles, « la sublimation d\'un arbre dans le vent » d\'après Marcel Duchamp.','test','Après que la seconde guerre mondiale ai pris fin, Marc Leflocon se lance dans le journalisme et en particulier dans le journalisme d\'art. \r\nIl est le créateur de ce nouveau format dans les années 60.','','fr','2013-12-08 18:43:32','0000-00-00 00:00:00'),(11,'Alexander Calder et la géométrie','Alexander Calder est un sculpteur et peintre américain né le 22 juillet 1898 à Lawnton près de Philadelphie et mort le 11 novembre 1976 à New York. Il est surtout connu pour ses mobiles, assemblages de formes animés par les mouvements de l\'air, et ses stabiles, « la sublimation d\'un arbre dans le vent » d\'après Marcel Duchamp.','test','Après que la seconde guerre mondiale ai pris fin, Marc Leflocon se lance dans le journalisme et en particulier dans le journalisme d\'art. \r\nIl est le créateur de ce nouveau format dans les années 60.','','fr','2013-12-08 18:43:32','0000-00-00 00:00:00');
/*!40000 ALTER TABLE `echos` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2013-12-15 22:01:43
