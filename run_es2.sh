#!/bin/sh

if [ $# -ne 3 ]; then
    echo Usage: $0 url user password 1>&2
    echo -n "Voulez vous poursuivre avec les paramètres par défaut? (y/n)"
    read response
    if [ "$response" = "y" ] || [ "$response" = "Y" ]; then
    	url="jdbc:mysql://localhost:3306/jocondelab"
    	user="root"
    	password="root"
    else
    	echo Abandon
    	exit 1
    fi
else
	url=$1
	user=$2
	password=$3
fi

curl -XDELETE 'localhost:9200/search'

curl -XPUT 'localhost:9200/_river/my_jdbc_river/_meta' -d '{
    "type" : "jdbc",
    "jdbc" : {
        "driver" : "com.mysql.jdbc.Driver",
        "url" : "'$url'",
        "user" : "'$user'",
        "password" : "'$password'",
        "sql" : "select t.id as _id, t.label as \"term.label\", t.lang as \"term.lang\", t.uri as \"term.uri\", t.dbpedia_uri as \"term.dbpedia_uri\", th.id as \"term.thesaurus_id\", th.label as \"term.thesaurus_label\" FROM core_term t LEFT JOIN core_thesaurus th ON t.thesaurus_id = th.id"
    },
    "index" : {
        "index" : "search",
        "type" : "autocomplete",

        "settings":{
            "analysis":{
              "analyzer":{
                "autocomplete":{
                  "type":"custom",
                  "tokenizer":"standard",
                  "filter":[ "standard", "lowercase", "stop", "kstem", "ngram" ] 
                }
              },
              "filter":{
                "ngram":{
                  "type":"ngram",
                  "min_gram":2,
                  "max_gram":15
                }
              }
            }
          }
    }
}'


# curl -XPUT 'localhost:9200/_river/my_jdbc_river/_meta' -d '{
#     "type" : "jdbc",
#     "jdbc" : {
#         "driver" : "com.mysql.jdbc.Driver",
#         "url" : "'$url'",
#         "user" : "'$user'",
#         "password" : "'$password'",
#         "sql" : "select t.id as _id, t.label as \"term.label\", t.lang as \"term.lang\", t.uri as \"term.uri\", t.dbpedia_uri as \"term.dbpedia_uri\", th.id as \"term.thesaurus_id\", th.label as \"term.thesaurus_label\", nt.notice_id as \"notice.id\" FROM core_term t LEFT JOIN core_thesaurus th ON t.thesaurus_id = th.id LEFT JOIN core_noticeterm nt ON nt.term_id = t.id LIMIT 100000"
#     },
#     "index" : {
#         "index" : "joconde",
#         "type" : "term_notice"
#     }
# }'


exit 0

